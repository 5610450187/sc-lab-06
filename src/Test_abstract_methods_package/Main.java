package Test_abstract_methods_package;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Parrot parrot = new Parrot("color ful");
		Cockato cockato = new Cockato("while");
		TeacherBird ta = new TeacherBird();
		
		ArrayList<Bird> birds  = new ArrayList<Bird>();
		birds.add(parrot);
		birds.add(cockato);
		
		
		for (Bird bird : birds) {
			
			System.out.println(bird);
			System.out.println(bird.voice());
			System.out.println(ta.orderToMakeVoice(cockato));
			System.out.println(ta.orderToMakeVoice(parrot));
			System.out.println(ta.orderToMakeVoice(bird));
		}

	}

}

package Test_abstract_methods_package;

public abstract class Bird {
	private String name;
	
	public Bird(String name){
		this.name = name;
	}
	public abstract String voice();
	
	public String toString(){
		return this.name;
	}
}

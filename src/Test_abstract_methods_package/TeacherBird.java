package Test_abstract_methods_package;

public class TeacherBird {
	public String orderToMakeVoice(Cockato cockato){
		return "Cockato " + cockato.voice();
		
	}
	public String orderToMakeVoice(Parrot parrot){
		return "Parrot " + parrot.voice();
		
	}
	public String orderToMakeVoice(Bird bird){
		return "Bird " + bird.voice();
	}
}
